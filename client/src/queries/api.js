import {
  getFirestore,
  onSnapshot,
  query,
  doc,
  getDoc,
  updateDoc
} from "firebase/firestore";

import {
  initializeApp
} from "firebase/app";

import {
  useMessagesStore,
} from "../stores/messages";

export const useFirestoreApi = () => {
  const firebaseConfig = {
    apiKey: "AIzaSyDNQ-ZK8uUDE9gjmauhdw3pjvnPkYARIo8",
    authDomain: "sunset-25d83.firebaseapp.com",
    projectId: "sunset-25d83",
    storageBucket: "sunset-25d83.appspot.com",
    messagingSenderId: "844097421891",
    appId: "1:844097421891:web:52d69c9f55ed777d1ecaf6",
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);

  return {
    db
  }
}
const {
  db
} = useFirestoreApi();

const docRef = doc(db, "Messages", "YZkDikLMYNL6KqMma7KM");

export const useAutoUpdatingStore = () => {
  const store = useMessagesStore();

  onSnapshot(docRef, (doc) => store.updateData(doc.data()))

  store.$subscribe((_, state) => {
    updateDoc(docRef, JSON.parse(JSON.stringify(state.courseData)), {
      merge: true
    });
  }, {
    deep: true
  })
}

export const getAllCalls = async () => {
  const {
    db
  } = useFirestoreApi()
  const q = query(
    doc(db, "Messages", "YZkDikLMYNL6KqMma7KM"),
  );

  return (await getDoc(q)).data();
}
