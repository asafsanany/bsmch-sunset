import {
  defineStore
} from 'pinia';

import {
  date
} from 'quasar'

export const useMessagesStore = defineStore('messages', {
  state: () => ({
    courseData: {
      Days: {}
    },
    selectedDate: date.formatDate(new Date(), 'YYYY/MM/DD')
  }),

  getters: {
    formattedSelectedDate(state) {
      return date.formatDate(state.selectedDate, 'DD-MM-YYYY');
    },

    today(state) {
      return (state?.courseData?.Days?.[state.formattedSelectedDate]) || {
        messages: [],
        commanders: []
      }
    },
  },

  actions: {
    updateData(newData) {
      this.courseData = newData;
    },
    updateToday(newToday) {
      ((this.courseData.Days ??= {})[this.formattedSelectedDate]) = newToday
    },
    changeSelectedDate(newDate) {
      this.selectedDate = newDate;
    },
    addCommander() {
      this.getCommandersByDate(this.formattedSelectedDate)
      .push({ name: "", students: [] })
    },
    addCall(commander){
      this.getCommandersByDate(this.formattedSelectedDate).find(
          currCommander => currCommander.name === commander.name)
        .students.push({ name: "", time: ""});
    },
    getDayByDate(date){
      return (((this.courseData.Days ??= {})[date]) ??= {messages:[], commanders:[]})
    },
    getCommandersByDate(date){
      return this.getDayByDate(date).commanders ??= []
    },
    // getMessagesByDate()
  }
})
