#!/bin/bash
echo  ' - Alright, here we go. trigerring auto deploy .. ..'  &&  echo

installDependencies() {
    echo " Installing global dependencies.."  &&       echo
    npm i -g @quasar/cli sails
}

setupServer() {
    echo  " Installing server dependencies .. .."
    cd ./app
    npm install
    cd ..
}

setupClient() {
    echo  " Installing application dependencies .. .."  &&  echo
    cd ./client
    npm install
    quasar build -m spa
    cd ..
}

copy() {
    echo  " Copying assets to server"  &&  echo
    mkdir -p ./app/views
    cp -R ./client/dist/spa/* ./app/views
}

installDependencies && setupServer && setupClient && copy
